# Navicat Keygen - How to build?

[中文版](how-to-build.zh-CN.md)

## 1. Prerequisites

1. Please make sure you have installed following libraries:

   * `capstone`
   * `keystone`
   * `rapidjson`

   If you use Ubuntu, you can install them by:

   ```console
   # install capstone
   $ sudo apt-get install libcapstone-dev -y

   # install keystone
   $ sudo apt-get install cmake -y 
   $ git clone https://github.com/keystone-engine/keystone.git
   $ cd keystone
   $ mkdir build
   $ cd build
   $ ../make-share.sh
   $ sudo make install
   $ sudo ldconfig

   # install rapidjson
   $ sudo apt-get install rapidjson-dev -y 
   # install ssl
   $ sudo apt install libssl-dev -y 
   # check c++17
   $ gcc -v --help 2> /dev/null | sed -n '/^ *-std=\([^<][^ ]\+\).*/ {s//\1/p}'
   ```

With ubuntu 18.04, install: 
   ```console
   # Check vesion
   $ gcc --version
   Output:
      gcc (Ubuntu 7.4.0-1ubuntu1~18.04) 7.4.0
      Copyright (C) 2017 Free Software Foundation, Inc.
      This is free software; see the source for copying conditions.  There is NO
      warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   
   # upgrade GCC and using multi version
   $ sudo apt install software-properties-common
   $ sudo add-apt-repository ppa:ubuntu-toolchain-r/test
   $ sudo apt install gcc-7 g++-7 gcc-8 g++-8 gcc-9 g++-9
   
   # Add version GCC
   $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9
   $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 80 --slave /usr/bin/g++ g++ /usr/bin/g++-8 --slave /usr/bin/gcov gcov /usr/bin/gcov-8
   $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 70 --slave /usr/bin/g++ g++ /usr/bin/g++-7 --slave /usr/bin/gcov gcov /usr/bin/gcov-7
   
   # using terminal make by version 
   $ sudo update-alternatives --config gcc
   
   OUTPUT:
   There are 3 choices for the alternative gcc (providing /usr/bin/gcc).

   Selection    Path            Priority   Status
   ------------------------------------------------------------
   * 0            /usr/bin/gcc-9   90        auto mode
     1            /usr/bin/gcc-7   70        manual mode
     2            /usr/bin/gcc-8   80        manual mode
     3            /usr/bin/gcc-9   90        manual mode
   
   Press <enter> to keep the current choice[*], or type selection number: 0   

2. Your gcc supports C++17 feature.

## 2. Build

```console
$ git clone -b linux --single-branch https://gitlab.com/NguyenXuanNam/navicat-keygen-linux.git
$ cd navicat-keygen-linux
$ make all
```

You will see executable files in `bin/` directory. 
